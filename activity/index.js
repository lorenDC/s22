/*
    Create functions which can manipulate our arrays.
*/

let userNames = ["Maria Dela Cruz", "Loren Dela Cruz", "Ervin Dela Cruz", "Jay Dela Cruz"]

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    
    function register(names){

        if(userNames.includes(names)){
            alert("Registration Failed. Username already exists!");
        } else {
            userNames.push(names);
            alert("Thank you for registering!");
        }
    }

    register("Loren Dela Cruz");
    register("Jay Dela Cruz");
    register("Grace Dela Cruz");
    register("Juan Dela Cruz");
    console.log("New users below")
    console.log(userNames);


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

    function includeFriend(username){

        if(userNames.includes(username)){
            friendsList.push(username);
            alert("You have added " + username + " as a friend!");
        } else {
            alert("User not found.");
        }
    };

    includeFriend("Loren Dela Cruz");
    includeFriend("Jay Dela Cruz");
    includeFriend("Grace Dela Cruz");
    includeFriend("Juan Dela Cruz");
    console.log("New users below")
    console.log(friendsList);

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

*/
    
    function displayList(){
        if (friendsList.length <= 0){
                alert("You currently have 0 friends. Add one first.");
        } else {
            friendsList.forEach(function(friend){
                console.log(friend);
            });        
        }
    }
    displayList();



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/

    function displayFriendsCount(){
        if(friendsList.length > 0){
            alert("You currently have " + friendsList.length);
            console.log("Friends count: " + friendsList.length)
        } else {
            alert("You have " + friendsList.length + " friends. Add one first.");
            console.log("Friends count: " + friendsList.length)
        }
    };

    displayFriendsCount();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/

    function removeFriend(){
        if(friendsList.length > 0){
            friendsList.pop();
        } else {
            alert("You have " + friendsList.length + " friends. Add one first.")
        }
    };
    removeFriend();
    console.log(friendsList);
    console.log("Updated Friends count: " + friendsList.length)

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function removeSelectedFriend(startIndex, deleteCount){
    if(friendsList.length <= 0){
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.splice(startIndex, deleteCount);
    }    
}
removeSelectedFriend(1,1)
console.log("Your current friends list after deleting specific user by index");
console.log(friendsList);
console.log("Updated Friends count: " + friendsList.length)
